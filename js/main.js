"use strict"
function loadVideos(){for(var e=document.getElementsByTagName("iframe"),t=0;t<e.length;t++)e[t].hasAttribute("data-src")&&e[t].setAttribute("src",e[t].getAttribute("data-src"))}function loadCode(){for(var codeTags=document.getElementsByTagName("code"),i=0;i<codeTags.length;i++)codeTags[i].hasAttribute("data-src")&&(codeTags[i].innerHTML=eval(codeTags[i].getAttribute("data-src")))}function shrinkElement(e){var t=e.classList
if(t.contains("pageTitle"))e.dataset.shrunk="true",e.firstElementChild.lastChild.nodeValue=""
else if(t.contains("navbarMenu")){e.dataset.shrunk="true"
var n=e.lastElementChild.previousElementSibling
for(n.hidden=!1,n=n.previousElementSibling;null!=n;)n.hidden=!0,n=n.previousElementSibling}else console.log("No classes found that can be shrunk.")}function expandElement(e){var t=e.classList
if(t.contains("pageTitle"))e.dataset.shrunk="false",e.firstElementChild.lastChild.nodeValue=" "+document.title
else if(t.contains("navbarMenu")){e.dataset.shrunk="false"
var n=e.lastElementChild.previousElementSibling
for(n.hidden=!0,n=n.previousElementSibling;null!=n;)n.hidden=!1,n=n.previousElementSibling}else console.log("No classes found that can be expanded.")}function expandedPriorityCmpDes(e,t){return t.getAttribute("data-expanded-priority")-e.getAttribute("data-expanded-priority")}function expandedPriorityCmpAs(e,t){return e.getAttribute("data-expanded-priority")-t.getAttribute("data-expanded-priority")}function shareLinkToElement(e){window.prompt("Share Link",window.location.hostname+window.location.pathname+"#"+e.id)}function smoothScrollTo(e,t,n){if(n=Math.round(n),0>n)return Promise.reject("bad duration")
if(0===n)return e.scrollTop=t.offsetTop,Promise.resolve()
var i=Date.now(),a=i+n,o=e.scrollTop,r=function(e,t,n){if(e>=n)return 0
if(n>=t)return 1
var i=(n-e)/(t-e)
return i*i*(3-2*i)}
return new Promise(function(n,s){var d=e.scrollTop,l=function(){var s=Date.now(),h=r(i,a,s),u=Math.round(o+(t.offsetTop-o)*h)
return e.scrollTop=u,a>s?(d=e.scrollTop,void setTimeout(l,0)):void n()}
setTimeout(l,0)})}var Navbar={initialised:!1,init:function(){this.initialised||(this.onResize(),window.addEventListener("resize",this.onResize.bind(this)),this.initialised=!0)},onResize:function(){clearTimeout(this.id),this.id=setTimeout(function(){for(var e=document.getElementsByClassName("navbar"),t=0;t<e.length;t++){for(var n=0,i=e[t].firstElementChild;null!=i;i=i.nextElementSibling)n+=i.offsetWidth+parseInt(window.getComputedStyle(i.parentElement).getPropertyValue("padding"))
if(e[t].offsetWidth>n){var a=[]
for(var i in e[t].children)a[i]=e[t].children[i]
a.sort(expandedPriorityCmpAs)
for(var o=-1,i=0;i<a.length;i++)"true"==a[i].dataset.shrunk&&(-1==o&&(o=a[i].getAttribute("data-expanded-priority")),a[i].getAttribute("data-expanded-priority")==o&&expandElement(a[i]))
n=0
for(var i=e[t].firstElementChild;null!=i;i=i.nextElementSibling)n+=i.offsetWidth+parseInt(window.getComputedStyle(i.parentElement).getPropertyValue("padding"))}if(e[t].offsetWidth<n){var a=[]
for(var i in e[t].children)a[i]=e[t].children[i]
a.sort(expandedPriorityCmpDes)
for(var r=-1,i=0;i<a.length;i++)(void 0==a[i].dataset.shrunk||"false"==a[i].dataset.shrunk)&&(-1==r&&(r=a[i].getAttribute("data-expanded-priority")),a[i].getAttribute("data-expanded-priority")==r&&shrinkElement(a[i]))}}},100)}},DropdownList={initialised:!1,autoScroll:!1,init:function(){if(!this.initialised){var e=document.getElementsByClassName("dropdownList")
if(e.length>=1){this.onResize(),window.addEventListener("resize",this.onResize)
for(var t=this.transitionEndEventName(),n=0;n<e.length;n++)for(var i=e[n].getElementsByClassName("header"),a=0;a<i.length;a++){var o=i[a].nextElementSibling
o.addEventListener(t,this.onTransitionEnd)}if(window.addEventListener("hashchange",this.onHashChange),window.location.hash){var r=document.getElementById(window.location.hash.slice(1,window.location.hash.length))
null===r&&(r=i[0]),this.onClick(r)}else this.onClick(i[0])
this.autoScroll=!0,this.initialised=!0}}},onResize:function(){for(var e=document.getElementsByClassName("dropdownList"),t=0;t<e.length;t++)for(var n=e[t].getElementsByClassName("header"),i=0;i<n.length;i++){var a=n[i].nextElementSibling,o=a.hidden
a.style.maxHeight="none",a.hidden=!1,a.style.maxHeight=getComputedStyle(a).height,a.hidden=o}},onClick:function(e){var t=e.nextElementSibling
t.hidden=!t.hidden
var n=e.lastElementChild.className.replace(/icon-[a-z\/-]* /i,"")
t.hidden?e.lastElementChild.className="icon-arrow-drop-down "+n:(e.lastElementChild.className="icon-arrow-drop-up "+n,this.autoScroll&&smoothScrollTo(document.body,e,1e3))},onHashChange:function(){var e=document.getElementById(window.location.hash.slice(1,window.location.hash.length))
null===e&&(e=headers[0]),DropdownList.onClick(e)},onTransitionEnd:function(e){"max-height"!=e.propertyName||e.srcElement.hidden||(e.srcElement.style.maxHeight="none",e.srcElement.style.maxHeight=getComputedStyle(e.srcElement).height)},transitionEndEventName:function(){var e=document.createElement("div"),t={transition:"transitionend",OTransition:"otransitionend",MozTransition:"transitionend",WebkitTransition:"webkitTransitionEnd"}
for(var n in t)if(void 0!=getComputedStyle(e)[n])return t[n]}}
window.addEventListener("DOMContentLoaded",function(){new Material({modules:["Dialog","Responsive","SideMenu","Ripple","DropdownMenu"]}),Navbar.init(),DropdownList.init()})
