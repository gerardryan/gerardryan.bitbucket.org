"use strict";

var Navbar = {
    initialised: false,
    init: function() {
        if (this.initialised) return;
        this.onResize();
        window.addEventListener("resize", this.onResize.bind(this));
        this.initialised = true;
    },
    onResize: function () {
        clearTimeout(this.id);
        this.id = setTimeout((function () {
            var navbars = document.getElementsByClassName("navbar");
            for (var i = 0; i < navbars.length; i++) {
                var totalElementsWidth = 0;
                for (var ele = navbars[i].firstElementChild; ele != null; ele = ele.nextElementSibling) {
                    totalElementsWidth += ele.offsetWidth + parseInt(window.getComputedStyle(ele.parentElement).getPropertyValue("padding"));
                }
                if (navbars[i].offsetWidth > totalElementsWidth) {

                    // sort by priority
                    var navbarElements = [];
                    for (var ele in navbars[i].children) navbarElements[ele] = navbars[i].children[ele];
                    navbarElements.sort(expandedPriorityCmpAs);

                    var expandPriority = -1;
                    for (var ele = 0; ele < navbarElements.length; ele++) {
                        if (navbarElements[ele].dataset.shrunk == "true") {
                            if (expandPriority == -1) {
                                expandPriority = navbarElements[ele].getAttribute("data-expanded-priority");
                            }
                            if (navbarElements[ele].getAttribute("data-expanded-priority") == expandPriority) {
                                expandElement(navbarElements[ele]);
                            }
                        }
                    }

                    // recalculate widths to see if change needs to be undone
                    totalElementsWidth = 0;
                    for (var ele = navbars[i].firstElementChild; ele != null; ele = ele.nextElementSibling) {
                        totalElementsWidth += ele.offsetWidth + parseInt(window.getComputedStyle(ele.parentElement).getPropertyValue("padding"));
                    }
                }
                if (navbars[i].offsetWidth < totalElementsWidth) {

                    // sort by priority
                    var navbarElements = [];
                    for (var ele in navbars[i].children) navbarElements[ele] = navbars[i].children[ele];
                    navbarElements.sort(expandedPriorityCmpDes);

                    var shrinkPriority = -1;
                    for (var ele = 0; ele < navbarElements.length; ele++) {
                        if (navbarElements[ele].dataset.shrunk == undefined || navbarElements[ele].dataset.shrunk == "false") {
                            if (shrinkPriority == -1) {
                                shrinkPriority = navbarElements[ele].getAttribute("data-expanded-priority");
                            }
                            if (navbarElements[ele].getAttribute("data-expanded-priority") == shrinkPriority) {
                                shrinkElement(navbarElements[ele]);
                            }
                        }
                    }
                }
            }
        }), 100)
    }
}

var DropdownList = {
    initialised: false,
    autoScroll: false,
    init: function () {
        if (this.initialised) return;
		
		var dropdownLists = document.getElementsByClassName("dropdownList");
		if (dropdownLists.length < 1) return;
		
        this.onResize();
        window.addEventListener("resize", this.onResize);

        var eventName = this.transitionEndEventName();
        for (var i = 0; i < dropdownLists.length; i++) {
            var headers = dropdownLists[i].getElementsByClassName("header");
            for (var j = 0; j < headers.length; j++) {
                var body = headers[j].nextElementSibling;
                body.addEventListener(eventName, this.onTransitionEnd);
            }
        }
		
		window.addEventListener("hashchange", this.onHashChange);
		
        if (window.location.hash) {
            var ele = document.getElementById(window.location.hash.slice(1, window.location.hash.length));

            if (ele === null) {
                ele = headers[0]; // default to first
            }

            this.onClick(ele);
        } else {
            this.onClick(headers[0]); // expand first
        }
        this.autoScroll = true;
        this.initialised = true;
    },
    onResize: function () {
        var dropdownLists = document.getElementsByClassName("dropdownList");
        for (var i = 0; i < dropdownLists.length; i++) {
            var headers = dropdownLists[i].getElementsByClassName("header");
            for (var j = 0; j < headers.length; j++) {
                var body = headers[j].nextElementSibling;
                var lastHidden = body.hidden;

                body.style.maxHeight = "none";
                body.hidden = false;
                body.style.maxHeight = getComputedStyle(body).height;

                body.hidden = lastHidden;
            }
        }
    },
    onClick: function (ele) {
        var body = ele.nextElementSibling
        body.hidden = !body.hidden
        var otherClasses = ele.lastElementChild.className.replace(/icon-[a-z/-]* /i, "");
        if (body.hidden) {
            ele.lastElementChild.className = "icon-arrow-drop-down " + otherClasses;
        } else {
            ele.lastElementChild.className = "icon-arrow-drop-up " + otherClasses;
            if (this.autoScroll) {
                smoothScrollTo(document.body, ele, 1000);
            }
        }
    },
	onHashChange: function() {
		var ele = document.getElementById(window.location.hash.slice(1, window.location.hash.length));

			if (ele === null) {
				ele = headers[0]; // default to first
			}

			DropdownList.onClick(ele);
	},
    onTransitionEnd: function (event) {

        // now we have time to load the element fully set correct max-height
        if (event.propertyName == "max-height" && !event.srcElement.hidden) {
            event.srcElement.style.maxHeight = "none";
            event.srcElement.style.maxHeight = getComputedStyle(event.srcElement).height;
        }
    },

    // sourced: http://stackoverflow.com/a/9090128/6306765
    transitionEndEventName: function () {
        var el = document.createElement('div');
        var transitions = {
            'transition': 'transitionend',
            'OTransition': 'otransitionend',
            'MozTransition': 'transitionend',
            'WebkitTransition': 'webkitTransitionEnd'
        };

        for (var t in transitions) {
            if (getComputedStyle(el)[t] != undefined) {
                return transitions[t];
            }
        }
        return undefined;
    }
}

window.addEventListener("DOMContentLoaded", function () {
    new Material({ modules: ["Dialog", "Responsive", "SideMenu", "Ripple", "DropdownMenu"] });
    Navbar.init();
    DropdownList.init();
});

// Source: https://varvy.com/pagespeed/defer-videos.html
function loadVideos() {
    var videoFrames = document.getElementsByTagName("iframe");
    for (var i = 0; i < videoFrames.length; i++) {
        if (videoFrames[i].hasAttribute("data-src")) {
            videoFrames[i].setAttribute("src", videoFrames[i].getAttribute("data-src"));
        }
    }
}

function loadCode() {
    var codeTags = document.getElementsByTagName("code");
    for (var i = 0; i < codeTags.length; i++) {
        if (codeTags[i].hasAttribute("data-src")) {
            codeTags[i].innerHTML = eval(codeTags[i].getAttribute("data-src"));
        }
    }
}

function shrinkElement(ele) {
    var classes = ele.classList;
    if (classes.contains("pageTitle")) {
        ele.dataset.shrunk = "true";
        ele.firstElementChild.lastChild.nodeValue = "";
    } else if (classes.contains("navbarMenu")) {
        ele.dataset.shrunk = "true";
        var navbarLink = ele.lastElementChild.previousElementSibling;
        navbarLink.hidden = false; // menu button

        navbarLink = navbarLink.previousElementSibling;
        while (navbarLink != null) {
            navbarLink.hidden = true;
            navbarLink = navbarLink.previousElementSibling;
        }
    } else {
        console.log("No classes found that can be shrunk.")
    }
}

function expandElement(ele) {
    var classes = ele.classList;
    if (classes.contains("pageTitle")) {
        ele.dataset.shrunk = "false";
        ele.firstElementChild.lastChild.nodeValue = " " + document.title;
    } else if (classes.contains("navbarMenu")) {
        ele.dataset.shrunk = "false";
        var navbarLink = ele.lastElementChild.previousElementSibling;
        navbarLink.hidden = true; // menu button

        navbarLink = navbarLink.previousElementSibling;
        while (navbarLink != null) {
            navbarLink.hidden = false;
            navbarLink = navbarLink.previousElementSibling;
        }
    } else {
        console.log("No classes found that can be expanded.")
    }
}

function expandedPriorityCmpDes(a, b) {
    return b.getAttribute("data-expanded-priority") - a.getAttribute("data-expanded-priority");
}

function expandedPriorityCmpAs(a, b) {
    return a.getAttribute("data-expanded-priority") - b.getAttribute("data-expanded-priority");
}

function shareLinkToElement(ele) {
    window.prompt("Share Link", window.location.hostname + window.location.pathname + '#' + ele.id);
}

/**
    Source: http://stackoverflow.com/a/25574313/6306765
    Smoothly scroll element to the top of a given element
    for the given duration

    Returns a promise that's fulfilled when done, or rejected if
    interrupted
 */
function smoothScrollTo(element, target, duration) {
    duration = Math.round(duration);
    if (duration < 0) {
        return Promise.reject("bad duration");
    }
    if (duration === 0) {
        element.scrollTop = target.offsetTop;
        return Promise.resolve();
    }

    var start_time = Date.now();
    var end_time = start_time + duration;

    var start_top = element.scrollTop;

    // based on http://en.wikipedia.org/wiki/Smoothstep
    var smooth_step = function (start, end, point) {
        if (point <= start) { return 0; }
        if (point >= end) { return 1; }
        var x = (point - start) / (end - start); // interpolation
        return x * x * (3 - 2 * x);
    }

    return new Promise(function (resolve, reject) {
        // This is to keep track of where the element's scrollTop is
        // supposed to be, based on what we're doing
        var previous_top = element.scrollTop;

        // This is like a think function from a game loop
        var scroll_frame = function () {

            // set the scrollTop for this frame
            var now = Date.now();
            var point = smooth_step(start_time, end_time, now);
            var frameTop = Math.round(start_top + ((target.offsetTop - start_top) * point));
            element.scrollTop = frameTop;

            // check if we're done!
            if (now >= end_time) {
                resolve();
                return;
            }
            previous_top = element.scrollTop;

            // schedule next frame for execution
            setTimeout(scroll_frame, 0);
        }

        // boostrap the animation process
        setTimeout(scroll_frame, 0);
    });
}