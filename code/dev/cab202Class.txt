int main(int argc, const char* argv[]) {
	//auto_save_screen = true;

	// screen and application setup
	setup_screen();
	override_screen_size(SCREEN_WIDTH, SCREEN_HEIGHT);
	gameStartTime = get_current_time();
	srand(SEED);

	// setup timers
	screenRefresh = create_timer(1000 / REFRESH_RATE);
	platformSpawn = create_timer(PLATFORM_SPAWN_TIME);
	resetJoystickX = create_timer(JOYSTICK_TIMEOUT);
	resetJoystickY = create_timer(JOYSTICK_TIMEOUT);
	bossSpawn = create_timer(rand() % BOSS_SPAWN_TIME_RANGE);

	onStartup();
	
	double loopStartTime;
	while (!quit) {
		loopStartTime = get_current_time();

		onUpdate();
		onDraw();

		deltaTime = get_current_time() - loopStartTime;
	}

	onCleanup();

	destroy_timer(screenRefresh);
	destroy_timer(platformSpawn);
	destroy_timer(resetJoystickX);
	destroy_timer(resetJoystickY);
	destroy_timer(bossSpawn);
	cleanup_screen();

	return EXIT_SUCCESS;
}