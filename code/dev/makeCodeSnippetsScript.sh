#!/bin/bash

file="../codeSnippets.js"

echo > $file

for f in *.txt
do
    name=$(echo $f | sed 's/.txt//g')
    echo "var " $name "= '\\" >> $file
    sed -b '
			s:\\:\\\\:g;
			s:\/:\\\/:g;
			s:'\'':\\'\'':g;
			s:<\([^>]*\)>:{\1}:g;
			s:\(^.*\)[\r|\n]:\1\\n\\:g;
			$s:^.*:&\\n\\:g;
		  ' $f >> $file
    echo >> $file
    echo "';" >> $file
done
